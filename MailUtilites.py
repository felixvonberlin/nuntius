#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Nuntius
# Copyright (C) 2019 Felix v. Oertzen
# felix@von-oertzen-berlin.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import poplib
import smtplib
import ssl
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.parser import Parser
from email.policy import SMTP


def parse_mail(mail):
    """Parses different kinds of e-mails and returns the text content."""
    content_type = mail.get_content_type().lower()
    if content_type == 'text/plain' or content_type == 'text/html':
        content = mail.get_payload(decode=True)
        content = content.decode("utf-8")
        return content
    elif content_type.startswith('multipart'):
        body_msg_list = mail.get_payload()
        for body_msg in body_msg_list:
            return parse_mail(body_msg)


class MailService:
    """This class is able to receive and send mails"""

    def __init__(self, smtp, pop, user, password):
        """Class initiation and place to configure mail settings

        Keyword arguments:
            smtp     -- smtp address of the mail server (STARTLS required)
            pop      -- pop3 address of the mail server (SSL required)
            user     -- user name for logging in (e.g. bot@test.com)
            password -- passphrase for signing in
        """

        self.__user = user
        self.__pop = pop
        self.__smtp = smtp
        self.__pwd = password

    def get_mails(self):
        """Returns an array of all new mails. They get deleted on the server."""
        mail_array = []
        pop3server = poplib.POP3_SSL(self.__pop)
        pop3server.user(self.__user)
        pop3server.pass_(self.__pwd)
        pop3info = pop3server.stat()
        mail_count = pop3info[0]
        for i in range(mail_count):
            for msg in pop3server.retr(i + 1)[1:-1]:
                msg = b'\r\n'.join(msg).decode('utf-8')
                msg = Parser(policy=SMTP).parsestr(msg)
                mail_array.append(msg)
                pop3server.dele(i + 1)
        pop3server.quit()
        return mail_array

    def send_mail(self, group_name, chat_id, sender, consignees, content=None, is_image=True):
        """Class initiation and place to configure mail settings

        Keyword arguments:
            group_name -- name of the group for mail subjecr
            chat_id    -- group id for managing replies correctly
            sender     -- the user name of the sender for information purpose
            consignees -- string array of e-mail-addresses of the mailing list
            content    -- array of texts, images or files
            is_image   -- if the mail does not contain a image, you should set False here for
                            changing the mime type
        """
        context = ssl.create_default_context()

        # Opens the connection to the mail server via STARTTLS
        with smtplib.SMTP(self.__smtp, 587) as server:
            #server.set_debuglevel(1)
            server.ehlo()
            server.starttls(context=context)
            server.ehlo()
            server.login(self.__user, self.__pwd)

            msg = MIMEMultipart('related')
            txt = MIMEText(sender + " wrote:\n\n", 'plain')
            msg.attach(txt)
            for snippet in content:
                if snippet is None:
                    continue
                if type(snippet) == str:
                    txt = MIMEText(snippet + "\n\n\n\n", 'plain')
                    msg.attach(txt)
                elif type(snippet) == bytes and is_image:
                    image = MIMEImage(snippet, "Image.jpg")
                    image.add_header('Content-Disposition', 'attachment',
                                     filename=('utf-8', '', 'Image ' + str(content.index(snippet)) + '.jpg'))
                    msg.attach(image)
                else:
                    payload = MIMEApplication(snippet)
                    msg.attach(payload)

            msg['From'] = "Nuntius <" + self.__user + ">"
            msg['To'] = "Someone"
            msg['Bcc'] = ", ".join(consignees)
            msg['Subject'] = "Message from \"" + group_name + "\" (id:" + str(chat_id) + ")"
            server.send_message(msg)
